/**
 * @author William Meng
 * @author Lilia Tang
 */
import java.awt.*;
import java.awt.geom.*;

abstract class GamePiece {
	Graphics2D board;
	Path2D sprite;
	Shape shape;
	Color color;
	char symbol;
	public static int width, height;
	protected int x, y;

	GamePiece(int x, int y) {
		this.x = x;
		this.y = y;
		width = height = 50;

		sprite = new Path2D.Double();
	}

	public final void draw(Graphics g) {
		board = (Graphics2D)g;
		board.setColor(color);
		board.fill(sprite);
	}
	public void updateSprite() {
		sprite.reset();
	}

	/**
	 * Sets the coordinate of the GamePiece
	 * using the given x and y values or Point.
	 */
	public void setCoord(int x, int y) {
		this.x = x;
		this.y = y;
		updateSprite();
	}
	public void setCoord(Point pt) {
		setCoord(pt.x, pt.y);
	}

	/** Print the current coordinates of the GamePiece */
	public void printCoord() {
		// Note that (0,0) is top left
		System.out.println("X: " + x);
		System.out.println("Y: " + y);
	}

	/**
	 *	Returns square of distance away of a piece from a given point
	 */
	public int distance(int i, int j) {
		int distance = (int) Math.pow(i-x, 2) + (int) Math.pow(j-y, 2);
		return distance;
	}

	/**
	 * Move to an adjacent square.
	 * @param direction can be horizontal, vertical, or diagonal
	 */
	public void moveAdjacent(Direction direction) {
		x += direction.getX();
		y += direction.getY();
		updateSprite();
	}
}
