JFLAGS = -g
JC = javac
JARFLAGS = -cvfm
JAR = hivolts.jar
MANIFEST = Manifest.txt

.SUFFIXES: .java .class

.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = Main.java

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class

jar: default
	$(RM) $(JAR); jar $(JARFLAGS) $(JAR) $(MANIFEST) *.class

all:
	javac *.java
