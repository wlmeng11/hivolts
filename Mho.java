/**
 * @author William Meng
 * @author Lilia Tang
 */
import java.awt.*;
import java.awt.geom.*;
import java.util.*;

/**
 * Each Mho is labeled with a hexadecimal digit that
 * increments for each instance.
 */
class Mho extends GamePiece {
	static int count = 1; // counts number of Mho instances
	final int id;
	boolean dead = false;

	Mho() {
		this(0, 0);
	}
	Mho(int x, int y) {
		super(x, y);
		id = count;
		String symbolString = Integer.toHexString(count);
		symbol = symbolString.charAt(0);

		count++;

		updateSprite();
	}

	public void updateSprite() {
		super.updateSprite();
		color = Color.RED;
		shape = new Ellipse2D.Double(x*width, y*height, width, height);
		sprite.append(shape, false);
	}

	private ArrayList<Direction> possibleMoves(int x, int y) {
		ArrayList<Direction> moves = new ArrayList<Direction>();
		moves.add(diagMove(x, y));
		moves.add(horzMove(x, y));
		moves.add(vertMove(x, y));
		return moves;
	}

	private Direction diagMove(int x, int y) {
		int absX = Math.abs(x);
		int absY = Math.abs(y);
		return new Direction(x/absX, y/absY);
	}

	private Direction horzMove(int x, int y) {
		int absX = Math.abs(x);
		int absY = Math.abs(y);
		return new Direction(x/absX, 0);
	}

	private Direction vertMove(int x, int y) {
		int absX = Math.abs(x);
		int absY = Math.abs(y);
		return new Direction(0, y/absY);
	}

	/**
	 *	Moves Mho towards a point 
	 *	according to the given rules for Mho movement
	 *	Based on a given point of attack
	 */
	public void move(Board board, Player player) {
		Direction dir = new Direction(0, 0);
		// distance from mho to player
		int xDist = player.x - x;
		int yDist = player.y - y;

		// Forced moves:
		// move vertically towards player
		if (xDist == 0)
			dir = (yDist < 0) ? Direction.NORTH : Direction.SOUTH;
		// move horizontally towards player
		else if (yDist == 0)
			dir = (yDist < 0) ? Direction.WEST : Direction.EAST;
		// Unforced moves:
		else {
			//System.out.println("\tunforced move");
			boolean canMove = false;
			// first try to move to empty square
			for (Direction move : possibleMoves(xDist, yDist)) {
				dir = move;
				int newX = x + dir.getX();
				int newY = y + dir.getY();
				if (board.isEmpty(newX, newY)) {
					//System.out.println("\tempty square");
					canMove = true;
					break; // proceed with moving
				}
			}
			// try to move onto fence
			if (!canMove) {
				//System.out.println("\ttry fence");
				for (Direction move : possibleMoves(xDist, yDist)) {
					dir = move;
					int newX = x + dir.getX();
					int newY = y + dir.getY();
					if (board.getPiece(newX, newY) instanceof Fence) {
						//System.out.println("\tmoving to fence");
						canMove = true;
						break; // proceed with moving
					}
				}
			}
			// try to move onto player
			// TODO: rules are ambiguous about moving onto player diagonally
			// I interpreted it to mean that Mho can only kill a player
			// horizontally or vertically, because moving onto the player
			// is not moving onto an empty square.
			boolean killDiagonally = false;
			// Uncomment the next line if otherwise
			//killDiagonally = true;
			if (!canMove && killDiagonally) {
				for (Direction move : possibleMoves(xDist, yDist)) {
					dir = move;
					int newX = x + dir.getX();
					int newY = y + dir.getY();
					if (board.getPiece(newX, newY) instanceof Player) {
						canMove = true;
						break; // proceed with moving
					}
				}
			}

			// otherwise stay put
			if (!canMove) {
				dir = Direction.STAY;
			}

			}

			int newX = x + dir.getX();
			int newY = y + dir.getY();
			if (board.getPiece(newX, newY) instanceof Fence) {
				// kill the mho
				System.out.println("\tMho " + id + " is dead");
				dead = true;
			}
			else if (board.getPiece(newX, newY) instanceof Player) {
				board.gameOver = true; // OMNOMNOM
				System.out.println("A Mho eats you!");
			}

			//System.out.println("\tMoving in : " + dir.getX() + "," + dir.getY());
			moveAdjacent(dir);
		}

		public static void main(String[] args) {
			// generate 12 Mho objects and print their symbols
			for (int i = 0; i < 12; i++)
				System.out.println(new Mho().symbol);
		}
	}
