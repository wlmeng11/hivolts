/**
 * @author William Meng
 * @author Lilia Tang
 * @author Ahmed Awadallah
 */
import java.awt.*;
import java.util.*;

class Board {
	private ArrayList<GamePiece> pieces;
	private Player player;
	public int width, height;
	public boolean gameOver;
	public boolean win;

	Board() {
		this(12, 12);
	}
	Board(int x, int y) {
		width = x;
		height = y;
		pieces = new ArrayList<GamePiece>();
		player = new Player();

		fillBoard();
	}

	/**
	 * Returns true if none of the game pieces are occupying
	 * the given coordinate.
	 */
	public boolean isEmpty(int x, int y) {
		for (GamePiece piece : pieces)
			if (piece.x == x && piece.y == y)
				return false;
		return true;
	}
	public boolean isEmpty(Point pt) {
		return isEmpty(pt.x, pt.y);
	}
	/**
	 * Opposite of isEmpty()
	 */
	public boolean isFilled(int x, int y) { return !isEmpty(x, y); }
	public boolean isFilled(Point pt) { return isFilled(pt.x, pt.y); }

	/**
	 *	Gets GamePiece at a given coordinate
	 */
	public GamePiece getPiece(int x, int y) {
		for (GamePiece piece : pieces)
			if (piece.x == x && piece.y == y)
				return piece;
		return null;
	}
	public GamePiece getPiece(Point pt) {
		return getPiece(pt.x, pt.y);
	}

	public ArrayList<GamePiece> getPieces() {
		return pieces;
	}

	/**
	 * Removes gamepiece at a given coordinate
	 */
	public void removePiece(int x, int y) {
		for (int i = 0; i < pieces.size(); i++) {
			GamePiece piece = pieces.get(i);
			if (piece.x == x && piece.y == y)
				pieces.remove(i);
		}
	}
	public void removeMho(int x, int y) {
		for (int i = 0; i < pieces.size(); i++) {
			GamePiece piece = pieces.get(i);
			if (piece.x == x && piece.y == y && piece instanceof Mho)
				pieces.remove(i);
		}
	}

	public boolean noMhos() {
		for (GamePiece piece : pieces) {
			if (piece instanceof Mho)
				return false;
		}
		return true;
	}

	/**
	 * Generate and return a random coordinate on the board,
	 * excluding the electric fence perimeter.
	 */
	public Point getRandomCoord() {
		Point pt = new Point();
		pt.x = 1 + (int)((width-2)*Math.random());
		pt.y = 1 + (int)((height-2)*Math.random());

		return pt;
	}
	/**
	 * Find and return a random empty coordinate on the board
	 */
	public Point getRandomEmptyCoord() {
		Point pt = getRandomCoord();
		if (isFilled(pt.x, pt.y)) // recurse until empty cell is found
			return getRandomEmptyCoord();
		return pt;
	}
	/**
	 * Finds a random cell, excluding perimeter, and sets
	 * the location of a given GamePiece.
	 */
	public void setRandomCoord(GamePiece piece) {
		Point pt = getRandomCoord();
		piece.setCoord(pt.x, pt.y);
	}
	/**
	 * Finds a random empty cell and sets the coordinate
	 * of a given GamePiece.
	 */
	public void setRandomEmptyCoord(GamePiece piece) {
		Point pt = getRandomEmptyCoord();
		piece.setCoord(pt.x, pt.y);
	}


	/**
	 * Sets the initial state of the board.
	 */
	public void fillBoard() {
		pieces.clear();

		if (width * height < 77) {
			System.out.println("Board is too small");
			return;
			//throw new IllegalArgumentException("Board is too small");
		}

		// Top and bottom borders
		for (int x = 1; x < width - 1; x++) {
			pieces.add(new Fence(x, 0));
			pieces.add(new Fence(x, height-1));
		}
		// Right and left borders
		for (int y = 0; y < height; y++) {
			pieces.add(new Fence(0, y));
			pieces.add(new Fence(width-1, y));
		}
		// 20 random Fences
		for (int i = 0; i < 20; i++) {
			Fence fence = new Fence();
			setRandomEmptyCoord(fence);
			pieces.add(fence);
		}
		// 12 random Mhos
		for (int i = 0; i < 12; i++) {
			Mho mho = new Mho();
			setRandomEmptyCoord(mho);
			pieces.add(mho);
		}
		// Randomize player
		setRandomEmptyCoord(player);
		pieces.add(player);
	}

	/**
	 * Prints an ASCII version of the board to the standard output.
	 */
	public void printBoard() {
		char[][] asciiBoard = new char[width][height];

		// Initialize each cell with a space character
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				asciiBoard[x][y] = " ".charAt(0);

		// Add each piece to asciiBoard
		for (GamePiece piece : pieces)
			asciiBoard[piece.x][piece.y] = piece.symbol;

		// Print asciiBoard
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++)
				System.out.print(asciiBoard[x][y]);
			System.out.println(); // linefeed after each row
		}
	}

	/**
	 * Prints board to the standard output, using
	 * "#" for a filled cell and " " for an empty cell.
	 */
	public void printBinaryBoard() {
		char empty = " ".charAt(0);
		char filled = "#".charAt(0);

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (isEmpty(x, y))
					System.out.print(empty);
				else
					System.out.print(filled);
			}
			System.out.println(); // linefeed after each row
		}
	}

	/**
	 * Moves the player in a given direction.
	 */
	public void movePlayer(Direction direction) {
		//System.out.println("\nMoving: " + direction.name());
		if (!isEmpty(player.x + direction.getX(), player.y + direction.getY())
				&& direction != Direction.STAY)
			gameOver = true;
		else {
			//System.out.println("Successful move");
		}

		player.moveAdjacent(direction);
		moveMhos();
		//printBoard();
		//player.printCoord();
	}

	/**
	 * Move all the Mhos, in order.
	 */
	public void moveMhos(){
		// make a list of Mhos
		ArrayList<Mho> mhos = new ArrayList<Mho>();
		for (GamePiece piece : pieces){
			if (piece instanceof Mho)
				mhos.add((Mho)piece);
		}
		// sort mhos
		Comparator<Mho> c = new Comparator<Mho>() {
			public int compare(Mho a, Mho b) {
				return (a.id > b.id) ? 1 : -1;
			}
		};
		Collections.sort(mhos, c);

		for (Mho mho : mhos) {
			//System.out.println("Moving mho " + mho.id + " at " + mho.x + "," + mho.y);
			mho.move(this, player);
			if (mho.dead) {
				removeMho(mho.x, mho.y);
			}
		}
	}

	/**
	 *	Moves player to a random cell that does not contain a fence
	 */
	public void jump() {
		Point pt = getRandomCoord();
		if (getPiece(pt.x, pt.y) instanceof Fence ||
				getPiece(pt.x, pt.y) instanceof Player)
			jump();
		else {
			if (getPiece(pt.x, pt.y) instanceof Mho)
				gameOver = true;
			player.setCoord(pt.x, pt.y);
		}
	}

	/**
	 *	Test method to move the player around
	 */
	public void testMove() {
		player.printCoord();
		movePlayer(Direction.NORTH);
		movePlayer(Direction.SOUTHEAST);
		movePlayer(Direction.NORTHEAST);
		movePlayer(Direction.SOUTHWEST);
		movePlayer(Direction.WEST);
		jump();
	}

	public static void main(String[] args) {
		Board board = new Board();
		board.printBoard();
		//board.printBinaryBoard();
		board.testMove();
	}
}
