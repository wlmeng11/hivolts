/**
 * Enumerates the possible directions in which
 * a game piece can move.
 * @author William Meng
 * @author Lilia Tang
 */
public class Direction {
	/* Note that the origin of the board is at the
	 * top left corner, so vertical directions are
	 * inverted.
	 */
	public static Direction NORTHEAST = new Direction(1, -1);
	public static Direction SOUTHEAST = new Direction(1, 1);
	public static Direction SOUTHWEST = new Direction(-1, 1);
	public static Direction NORTHWEST = new Direction(-1, -1);
	public static Direction NORTH = new Direction(0, -1);
	public static Direction EAST = new Direction(1, 0);
	public static Direction SOUTH = new Direction(0, 1);
	public static Direction WEST = new Direction(-1, 0);
	public static Direction STAY = new Direction(0, 0);

	private final int x, y;

	Direction(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() { return x; }
	public int getY() { return y; }
}
