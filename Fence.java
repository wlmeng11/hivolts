/**
 * @author William Meng
 */
import java.awt.*;
import java.awt.geom.*;

class Fence extends GamePiece {
	Fence() {
		this(0, 0);
	}
	Fence(int x, int y) {
		super(x, y);
		symbol = "F".charAt(0);

		updateSprite();
	}

	public void updateSprite() {
		super.updateSprite();
		color = Color.YELLOW;
		shape = new Ellipse2D.Double(x*width, y*height, width, height);
		sprite.append(shape, false);
	}
}
