import java.awt.Color;
import java.awt.Graphics;



import javax.swing.JFrame;
//30 pixels per grid square
// there is 30 pixel deficit at the top
//The is a 8 pixel deficit from the right
//Default Hshift is 3 and default Vshift is 11
//A player must move 30 pixels to move one square
public class HivEnvironment extends JFrame {
	static int[] Gridx = new int[24];
	static int[] Gridy = new int[24];
	int inputx;
	int inputy;
	static int xstart=57;
	static int ystart=82;
	int setSize=29;
	public HivEnvironment () {
		init();
		setBackground(Color.black);
	}
	
	public HivEnvironment(Graphics g, int x, int y){
		inputx = x;
		inputy = y;
		g.setColor(Color.yellow);
		g.fillRect(Gridx[x], Gridy[y], setSize, setSize);
	}
	
	public int getInputX(){
		return inputx;
	}
	
	public void setInputX(int newinputx){
		inputx=newinputx;
	}

	
	public void init() {
		setSize(800, 830);
		CreateGridArray();
		
		
	}
	
	public void paint(Graphics g) {
		
		Background(g);
		Grid(g);
		ExamplePlayer(g);
		createElecPerim(g);
		System.out.println(Gridx[1]);
		RandomMath();
		createRandomFences(g);
		//HivEnvironment Player = new HivEnvironment(g, 2, 2);
		//Player.Move(Player);
	}
	
	public static void createElecPerim(Graphics g){
		for(int i = 0; i<=23; i+=2){
			new HivEnvironment(g, 1+i, 1);
			new HivEnvironment(g, 1+i, 23);
		}
		for(int i = 0; i<=23; i+=2){
			new HivEnvironment(g, 1, 1+i);
			new HivEnvironment(g, 23, 1+i);
		}
		
		
	}
	public static void createRandomFences(Graphics g){
		for(int i = 0; i<20; i++){
			new HivEnvironment(g, RandomMath(), RandomMath());
		}
	}
	public static int RandomMath(){
		
		double randomNumber= Math.random()*23;
		if((int)randomNumber<2 || (int)randomNumber>22){
			do{
				randomNumber=randomNumber+1;
			}
			while((int)randomNumber<2);
		}
		System.out.println((int)randomNumber);
		return (int)randomNumber;
	}
	public static void CreateGridArray(){
		Gridx[0]=0;
		Gridy[0]=0;
		for(int i=1; i<=23;i++){
			Gridx[i]=xstart;
			xstart+=30;
		}
		for(int i=1; i<=23;i++){
			Gridy[i]=ystart;
			ystart+=30;
		}
		
	}
	public static void Background(Graphics g){
		g.fillRect(0, 0, 800, 830);
	}
	
	public static void Grid(Graphics g){
		//Color is temporary its for our visual purpose
		int Hshift=8;
		int Vshift=11;
		g.setColor(Color.white);
		for(int i = 0; i<720; i+=30){
			g.drawLine(48+i+Hshift, 70+Vshift, 48+i+Hshift, 760+Vshift);
		}
		for(int i = 0; i<720; i+=30){
			g.drawLine(48+Hshift, 70+i+Vshift, 738+Hshift,70+i+Vshift);
		}
		
		
		
	}
	public static void ExamplePlayer(Graphics g){
		g.setColor(Color.green);
		g.fillRect(48+9,70+12,29,29);
	}
	public void Move(HivEnvironment Player){
		inputx=inputx+1;
		repaint();
		setBackground(Color.black);
	}
	
}