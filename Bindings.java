/**
 * @author William Meng
 * @author Gabe Mechali
 */
import java.awt.event.*;
import javax.swing.*;

public enum Bindings {
	W ('w', Direction.NORTH),
	X ('x', Direction.SOUTH),
	D ('d', Direction.EAST),
	A ('a', Direction.WEST),
	Q ('q', Direction.NORTHWEST),
	E ('e', Direction.NORTHEAST),
	Z ('z', Direction.SOUTHWEST),
	C ('c', Direction.SOUTHEAST),
	S ('s', Direction.STAY),
	J ('j', null),
	UP (KeyEvent.VK_UP, Direction.NORTH),
	DOWN (KeyEvent.VK_DOWN, Direction.SOUTH),
	LEFT (KeyEvent.VK_LEFT, Direction.WEST),
	RIGHT (KeyEvent.VK_RIGHT, Direction.EAST);

	private final Direction direction;
	public final KeyStroke keyStroke;

	Bindings(char key, Direction d) {
		direction = d;
		keyStroke = KeyStroke.getKeyStroke(key);
	}

	Bindings(int keycode, Direction d) {
		direction = d;
		keyStroke = KeyStroke.getKeyStroke(keycode, 0);
	}

	public char getKeyChar() {
		return keyStroke.getKeyChar();
	}

	public Direction getDirection() {
		return direction;
	}
}
