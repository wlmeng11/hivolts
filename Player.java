/**
 * @author William Meng
 */
import java.awt.*;
import java.awt.geom.*;

class Player extends GamePiece {
	Player() {
		this(0, 0);
	}
	Player(int x, int y) {
		super(x, y);
		symbol = "+".charAt(0);

		updateSprite();
	}

	public void updateSprite() {
		super.updateSprite();
		color = Color.BLUE;
		shape = new Ellipse2D.Double(x*width, y*height, width, height);
		sprite.append(shape, false);
	}
}
