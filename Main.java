/**
 * @author William Meng
 * @author Gabe Mechali
 * @author Ahmed Awadallah
 * @author Lilia Tang
 */
import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

public class Main extends JFrame implements ComponentListener, MouseListener {
	private int height, width;
	private double scaleFactor = 1;
	Board board;

	Main() {
		board = new Board();
		height =  board.height * GamePiece.height;
		width = board.width * GamePiece.width;

		bindKeys();

		init();
		addComponentListener(this);
		addMouseListener(this);
	}

	public void init() {
		setTitle("Hivolts");
		setSize(width, height);
		setBackground(Color.BLACK);
		setVisible(true);
		repaint();

	}

	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		g2d.translate(getInsets().left, getInsets().top);

		//System.out.println("scale factor: " + scaleFactor);
		g2d.scale(scaleFactor, scaleFactor);

		if (board.gameOver) {
			g.setColor(new Color(0, 0, 0, 200));
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(Color.YELLOW);
			g.drawString("You Lost! Click anywhere to play again...",
					(getContentPane().getWidth()/2)-80,
					(getContentPane().getHeight()/2));
		}
		else if (board.noMhos()) {
			g.setColor(new Color(0, 0, 0, 200));
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(Color.YELLOW);
			g.drawString("You Won! Click anywhere to play again...",
					(getContentPane().getWidth()/2)-80,
					(getContentPane().getHeight()/2));
		}
		else {
			// fill background to avoid artifacting
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());

			// anti-aliasing
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			for (GamePiece piece : board.getPieces()) {
				piece.draw(g);
			}
		}
	}

	private void bindKeys() {
		//System.out.println("Binding keys");

		InputMap inputMap = getRootPane().getInputMap();
		ActionMap actionMap = getRootPane().getActionMap();

		for (final Bindings b : Bindings.values()) {
			Action action;
			if (b.getKeyChar() == 'j') {
				action = new AbstractAction() {
					public void actionPerformed(ActionEvent e){
						board.jump();
						repaint();
					}
				};
			}
			else {
				action = new AbstractAction() {
					public void actionPerformed(ActionEvent e){
						//System.out.println("got key: " + b.getKeyChar());
						board.movePlayer(b.getDirection());
						repaint();
					}
				};
			}

			inputMap.put(b.keyStroke, action);
			actionMap.put(action, action);
		}
	}

	public void componentHidden(ComponentEvent e) {}
	public void componentMoved(ComponentEvent e) {}
	public void componentShown(ComponentEvent e) {}
	public void componentResized(ComponentEvent e) {
		fitToWindow();
	}

	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseClicked(MouseEvent e) {
		if (board.gameOver || board.noMhos()) {
			//System.out.println("Mouse Clicked");
			board.gameOver = false;
			board.fillBoard();
			repaint();
		}
	}

	public void fitToWindow() {
		double windowWidth = (double)getContentPane().getWidth();
		double windowHeight = (double)getContentPane().getHeight();
		width = board.width * GamePiece.width;
		height =  board.height * GamePiece.height;
		//System.out.println("Window width: "+ windowWidth);
		//System.out.println("Window height: "+ windowHeight);

		// compare current aspect ratio to original aspect ratio
		if (windowHeight/windowWidth >= (double)height/width) {
			scaleFactor = windowWidth/width;
		}
		else {
			scaleFactor = windowHeight/height;
		}
		repaint();
	}

	public void resizeBoard() {
		String width = System.console().readLine("Enter board width: ");
		String height = System.console().readLine("Enter board height: ");
		try {
			board.width = Integer.parseInt(width);
			board.height = Integer.parseInt(height);
			board.fillBoard();
			fitToWindow();
		} catch (NumberFormatException e) {
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Main f = new Main();
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);

		while (true) {
			f.resizeBoard();
		}
	}
}
